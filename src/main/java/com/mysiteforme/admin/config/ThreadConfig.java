package com.mysiteforme.admin.config;

import java.util.concurrent.Executor;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
/*
 *1. 利用EnableAsync来开启Springboot对于异步任务的支持
 *2. 配置类实现接口AsyncConfigurator，返回一个ThreadPoolTaskExecutor线程池对象。
 */

@Configuration
@EnableAsync
public class ThreadConfig implements AsyncConfigurer {
    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
//        核心线程数
        executor.setCorePoolSize(80);
//        最大线程数
        executor.setMaxPoolSize(1000);
//        线程池所用的缓冲队列大小
        executor.setQueueCapacity(500);
//        线程保活时间（单位秒）
        executor.setKeepAliveSeconds(30000);
        executor.initialize();
        return executor;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return null;
    }

}