package com.mysiteforme.admin.service.impl;

import com.google.common.collect.Lists;
import com.mysiteforme.admin.entity.MyDatatTest;
import com.mysiteforme.admin.dao.MyDatatTestDao;
import com.mysiteforme.admin.exception.MyException;
import com.mysiteforme.admin.service.MyDatatTestService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.util.RandomValueUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 测试数据表 服务实现类
 * </p>
 *
 * @author wangl
 * @since 2020-05-05
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class MyDatatTestServiceImpl extends ServiceImpl<MyDatatTestDao, MyDatatTest> implements MyDatatTestService {

   List<MyDatatTest> list = new ArrayList<MyDatatTest>();
    @Override
    public Long createMyDatates(Long num) {
        Long  n = 0L;
        for (int i = 0; i <= num ; i++) {
            MyDatatTest date=new MyDatatTest();
            date.setName(RandomValueUtil.getChineseName());
            date.setAge(RandomValueUtil.getNum(0,120));
            date.setTel(RandomValueUtil.getTelephone());
            date.setRemake(RandomValueUtil.getChineseName()+":"+RandomValueUtil.getTelephone());
            list.add(date);
        }
        List<List<MyDatatTest>> lists = Lists.partition(list, 20000);
        for (List<MyDatatTest> li :lists) {
               boolean b = this.insertBatch(li);
            n+=li.size();
        }

        return n;
    }
}
