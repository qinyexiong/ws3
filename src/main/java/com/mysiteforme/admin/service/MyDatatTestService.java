package com.mysiteforme.admin.service;

import com.mysiteforme.admin.entity.MyDatatTest;
import com.baomidou.mybatisplus.service.IService;
/**
 * <p>
 * 测试数据表 服务类
 * </p>
 *
 * @author wangl
 * @since 2020-05-05
 */
public interface MyDatatTestService extends IService<MyDatatTest> {

    Long createMyDatates(Long num);
}
