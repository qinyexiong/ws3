package com.mysiteforme.admin.dao;

import com.mysiteforme.admin.entity.MyDatatTest;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 测试数据表 Mapper 接口
 * </p>
 *
 * @author wangl
 * @since 2020-05-05
 */
public interface MyDatatTestDao extends BaseMapper<MyDatatTest> {

}
