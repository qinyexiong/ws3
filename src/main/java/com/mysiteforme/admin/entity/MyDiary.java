package com.mysiteforme.admin.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.mysiteforme.admin.base.DataEntity;

/**
 * <p>
 * 日记
 * </p>
 *
 * @author wangl
 * @since 2020-04-25
 */
@TableName("my_diary")
public class MyDiary extends DataEntity<MyDiary> {

    private static final long serialVersionUID = 1L;

    /**
     * 备注
     */
	private String remake;

	public String getRemake() {
		return remake;
	}

	public void setRemake(String remake) {
		this.remake = remake;
	}


	@Override
	public String toString() {
		return "MyDiary{" +
			", remake=" + remake +
			"}";
	}
}
