package com.mysiteforme.admin.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.mysiteforme.admin.base.DataEntity;

/**
 * <p>
 * 测试数据表
 * </p>
 *
 * @author wangl
 * @since 2020-05-05
 */
@TableName("my_datat_test")
public class MyDatatTest extends DataEntity<MyDatatTest> {

    private static final long serialVersionUID = 1L;

    /**
     * 名字
     */
	private String name;
    /**
     * 年龄
     */
	private Integer age;
    /**
     * 手机
     */
	private String tel;
    /**
     * 备注
     */
	private String remake;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getRemake() {
		return remake;
	}

	public void setRemake(String remake) {
		this.remake = remake;
	}


	@Override
	public String toString() {
		return "MyDatatTest{" +
			", name=" + name +
			", age=" + age +
			", tel=" + tel +
			", remake=" + remake +
			"}";
	}
}
