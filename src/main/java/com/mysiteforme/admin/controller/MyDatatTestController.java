package com.mysiteforme.admin.controller;

import com.xiaoleilu.hutool.date.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mysiteforme.admin.entity.MyDatatTest;
import com.mysiteforme.admin.service.MyDatatTestService;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import com.mysiteforme.admin.annotation.SysLog;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 测试数据表  前端控制器
 * </p>
 *
 * @author wangl
 * @since 2020-05-05
 */
@Controller
@RequestMapping("/admin/myDatatTest")
public class MyDatatTestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(MyDatatTestController.class);

    @Autowired
    private MyDatatTestService myDatatTestService;

    @GetMapping("list")
    @SysLog("跳转测试数据表列表")
    public String list(){
        return "/admin/myDatatTest/list";
    }

    @PostMapping("list")
    @ResponseBody
    @SysLog("请求测试数据表列表数据")
    public LayerData<MyDatatTest> list(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                      @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                      ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<MyDatatTest> layerData = new LayerData<>();
        EntityWrapper<MyDatatTest> wrapper = new EntityWrapper<>();
        wrapper.eq("del_flag",false);
        if(!map.isEmpty()){
            String name = (String) map.get("name");
            if(StringUtils.isNotBlank(name)) {
                wrapper.like("name",name);
            }else{
                map.remove("name");
            }

            String age = (String) map.get("age");
            if(StringUtils.isNotBlank(age)) {
                wrapper.like("age",age);
            }else{
                map.remove("age");
            }

            String tel = (String) map.get("tel");
            if(StringUtils.isNotBlank(tel)) {
                wrapper.like("tel",tel);
            }else{
                map.remove("tel");
            }

        }
        Page<MyDatatTest> pageData = myDatatTestService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setData(pageData.getRecords());
        layerData.setCount(pageData.getTotal());
        return layerData;
    }

    @GetMapping("add")
    @SysLog("跳转新增测试数据表页面")
    public String add(){
        return "/admin/myDatatTest/add";
    }

    @PostMapping("add")
    @SysLog("保存新增测试数据表数据")
    @ResponseBody
    public RestResponse add(MyDatatTest myDatatTest){
        myDatatTestService.createMyDatates(20000L);
        return RestResponse.success();
    }

    @GetMapping("edit")
    @SysLog("跳转编辑测试数据表页面")
    public String edit(Long id,Model model){
        MyDatatTest myDatatTest = myDatatTestService.selectById(id);
        model.addAttribute("myDatatTest",myDatatTest);
        return "/admin/myDatatTest/edit";
    }

    @PostMapping("edit")
    @ResponseBody
    @SysLog("保存编辑测试数据表数据")
    public RestResponse edit(MyDatatTest myDatatTest){
        if(null == myDatatTest.getId() || 0 == myDatatTest.getId()){
            return RestResponse.failure("ID不能为空");
        }
        myDatatTestService.updateById(myDatatTest);
        return RestResponse.success();
    }

    @PostMapping("delete")
    @ResponseBody
    @SysLog("删除测试数据表数据")
    public RestResponse delete(@RequestParam(value = "id",required = false)Long id){
        if(null == id || 0 == id){
            return RestResponse.failure("ID不能为空");
        }
        MyDatatTest myDatatTest = myDatatTestService.selectById(id);
        myDatatTest.setDelFlag(true);
        myDatatTestService.updateById(myDatatTest);
        return RestResponse.success();
    }

}