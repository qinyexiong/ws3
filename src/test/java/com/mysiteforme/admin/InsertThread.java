package com.mysiteforme.admin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

public class InsertThread extends Thread {
    public void run() {
        System.out.println("线程名称:" + Thread.currentThread().getName());
        String url = "jdbc:mysql://localhost:3306/mysiteforme"; //数据库连接地址
        String name = "com.mysql.jdbc.Driver";
        String user = "root";
        String password = "123456";//密码
        Connection conn = null;
        try {
            Class.forName(name);
            conn = DriverManager.getConnection(url, user, password);
            conn.setAutoCommit(false);//关闭自动提交
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        long begin = new Date().getTime();
        String sql = "INSERT INTO test1 (id,name,age) VALUES(?,?,?)";
        try {
            StringBuilder sqls = new StringBuilder();
            conn.setAutoCommit(false);
            PreparedStatement pst = conn.prepareStatement(sql);
            //编写事务
            for (int i = 1; i <= 10; i++) {

                for (int j = 1; j <= 100000; j++) {
                    pst.setString(1, j + Thread.currentThread().getName() + System.currentTimeMillis());
                    pst.setString(2, "张三" + j);
                    pst.setString(3, "age" + j);

                    pst.addBatch();//批量添加信息
                }
                // 执行批量操作
                pst.executeBatch();
                // 提交事务
                conn.commit();
            }
            pst.close();
            conn.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // 结束时间
        Long end = new Date().getTime();
        // 耗时
        System.out.println("100万条数据插入花费时间 : " + (end - begin) / 1000 + " s" + "  插入完成");
    }


}

