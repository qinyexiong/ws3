package com.mysiteforme.admin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TestLamada {
    public static void main(String[] args) {

        ArrayList<User> lst = new ArrayList();
        User user = new User();
        user.setName("张三");
        user.setAge("22");
        lst.add(user);
        User user1 = new User();
        user1.setName("李四");
        user1.setAge("23");
        lst.add(user1);
        User user2 = new User();
        user2.setName("王五");
        user2.setAge("24");
        lst.add(user2);

        User user3 = new User();
        user3.setName("张三");
        user3.setAge("25");
        lst.add(user3);
        // 名字分组
        Map<String, User> collect = lst.stream().collect(Collectors.toMap(User::getName, Function.identity(), (key1, key2) -> key2));

        LinkedHashMap<String, User> collect1 = lst.stream().collect(Collectors.toMap(User::getName, Function.identity(), (key1, key2) -> key2, LinkedHashMap::new));
        // 名字分组
        Map<String, User> collect2 = lst.stream().collect(Collectors.toMap(u -> u.getName() + u.getAge()
                , Function.identity(), (key1, key2) -> key2));

        System.out.println(collect.toString());

    }


}
